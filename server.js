const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const DashboardPlugin = require('webpack-dashboard/plugin');

const config = require('./webpack.config.js');
const compiler = webpack(config);

compiler.apply(new DashboardPlugin());

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
	noInfo: true, publicPath: config.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler));

reg = /cone.html$/

let status = {
	coneConnected: false,
	coneID: null
}

io.on('connection', function(socket){
	console.log(socket.handshake.headers.referer);
  	console.log('A user connected');
  	console.log(socket.id);
  	if( socket.handshake.headers.referer.match(reg) ) {
		io.emit('check', { status });
	}

	socket.on('transfer', function(msg){
    	console.log(msg);
    	io.emit('sent', msg);
  	});

	socket.on('openDoor', function(msg){
    	console.log(msg);
    	io.emit('openedDoor', msg);
  	});

	socket.on('closeDoor', function(msg){
    	console.log(msg);
    	io.emit('closedDoor', msg);
  	});

  	socket.on('cone connection', function() {
		console.log("Cone connected");
  		status.coneConnected = true;
  		status.coneId = socket.id;
  	});

  	socket.on('disconnect', (reason) => {
  		console.log(reason);
  		if( socket.id == status.coneId ) {
  			console.log("Cone disconnected");
			status.coneConnected = false;
			status.coneId = null;
  		}
    
  	});
});

// Serve the files on port 3000.
server.listen(80, function () {
	console.log('Server running on port 80!\n');
});
