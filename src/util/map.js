// Not using this right now
function mapNumber( number, a, b, c, d ) {
	if (number > a) { number = a; }
	if (number < b) { number = b; }

	const x = number;
	const Y = c + ( d - c ) * (( x - a ) / ( b - a));
	//console.log("x: " + number + " - mapped: " + Y );
	return Y;
}

export default mapNumber;
