import io from 'socket.io-client';
import Scene from './components/Scene.js'

import 'normalize.css'
import './css/app.css'

const app = document.querySelector('#app');

const socket = io();

let scene = new Scene( "PHONE" );

scene.setup( socket );
scene.start( app );
scene.animate();


