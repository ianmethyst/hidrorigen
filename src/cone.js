import io from 'socket.io-client';
import Scene from './components/Scene.js';

import 'normalize.css'
import './css/app.css'

const socket = io();

let check;
let scene;

socket.on('check', function( data ) {
	console.log( data );
	if ( !check ) {
		check = data.status.coneConnected;

		console.log(check);
		if ( check === false ) {

			socket.emit('cone connection', { connection: true }); 

			scene = new Scene( "CONE" );
		} else { 
			console.log("Oh, so you want be a cone, but there's room for just one cone in this town");
		}

		if ( scene ) {
			scene.setup ( socket );
			scene.start ( app );
			scene.animate();
		}
	}
});


