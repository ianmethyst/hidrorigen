import * as THREE from 'three';
import DiffCamEngine from '../vendor/DiffCamEngine.js';
import Cell from './Cell.js'; 
import Attractor from './Attractor.js';

class Scene {
  constructor( client ) {
    this.webcamCheckMotion = this.webcamCheckMotion.bind(this);

    /*
     * Client posible values:
     *  PHONE
     *  CONE
     */

    this.client = client;

    this.cells = [];

    //Attractor
    this.A = null; 

    this.frontLight = {
      light: null,
      hue: 0.597,
      saturation: 0,
      lightning: 0.5,
      color: new THREE.Color()
    }

    this.exitAvail = false;

    if ( client === "PHONE" ) {
      this.raycaster = {
        R: null,  
        mouse:  new THREE.Vector2(10000, 10000),
        intersect:  true
      }


      this.accel = {
        x: 0,
        y: 0
      }

      this.topLight = {
        light: null,
        intensity: 0,
        color: new THREE.Color()

      }


    } else {
      // client === "CONE" 
      this.receivedData = null;

      this.frontLight.hue = 0.3333333;
      this.frontLight.blink = false;

      DiffCamEngine.init({
        initSuccessCallback: this.initWebcamSuccess,
        initErrorCallback: this.initWebcamError,
        captureCallback: this.webcamCheckMotion
      });

      this.timeout;
    }

    this.physics = {
      friction: null,
      frictionNormal: 1,
      frictionConstant: 0.0025,
      frictionMag: 0  
    }

    this.background = {
      ID:  0,
      plane: null,
      hue: 0.583,
      saturation: 0,
      color: new THREE.Color()
    }


    this.masterCell = null;

    this.addEventListeners();


    this.animate = this.animate.bind(this);
    this.setAccel = this.setAccel.bind(this);

  }

  initWebcamSuccess() {
    DiffCamEngine.start();
  }

  initWebcamError() {
    alert('Something went wrong.');
  }

  webcamCheckMotion( payload, context ) {
    if ( payload.score > 80 ) {
      if (this.exitAvail === false) {
        this.exitAvail = true;

        this.openDoor();
      }

      clearTimeout(this.timeout);

      this.timeout = setTimeout(() => {
        this.closeDoor();
        this.exitAvail = false
      }, 1500 );
    }
  }

  openDoor() {
    this.socket.emit('openDoor', {
      exitAvail:  true
    });
  }

  closeDoor() {
    this.socket.emit('closeDoor', {
      exitAvail: false
    });
  }

  setup( socket ) {
    this.socket = socket;
    this.scene = new THREE.Scene();

    this.initCamera();
    this.initLights();
    this.initRenderer();
    this.initBackground();

    if  (this.client === "PHONE") {

      this.initRaycaster();

      this.cells[0] = new Cell( -1.85, 2.5, null, null );
      this.cells[1] = new Cell( 1.85, 2.5, null, null );
      this.cells[2] = new Cell( -1.85, -2.5, null, null );
      this.cells[3] = new Cell( 1.85, -2.5, null, null );

      for( let i = 0; i < this.cells.length; i++) {
        this.scene.add(this.cells[i].mesh);
      }

      this.A = new Attractor( 0, 0, null, 1);

    } else {
      // this.client === "CONE"
      this.dataRecieved = false;
      this.A = new Attractor( null, null, 0, 1 );
      this.water();
    }

    this.audio( this );
  }

  setAccel( x, y ) {
    this.accel.x = x;
    this.accel.y = y;
  }

  addEventListeners() {
    if ( this.client === "PHONE" ) {
      document.addEventListener( 'touchstart', ( event ) => this.onTouchStart( event ), false );
      window.addEventListener( 'devicemotion', ( event ) => this.onDeviceMotion( event ), true);
    }
    window.addEventListener( 'resize', () => this.onWindowResize(), false );
  }

  initCamera() {
    this.camera = new THREE.PerspectiveCamera( 
      75,
      window.innerWidth/window.innerHeight, 
      0.1,
      1000
    );

    this.camera.position.z = 10;
  }

  blink() {
    this.frontLight.light.color.setHSL(this.frontLight.hue, this.frontLight.saturation, 0.50);
    if( this.frontLight.blink ) {
      if ( this.frontLight.saturation < 1 ) {
        this.frontLight.saturation += 0.01;
      } else {
        this.frontLight.blink = false;
      }
    } else {
      if( this.frontLight.saturation > 0 ) {
        this.frontLight.saturation -= 0.01;
      }
    }
  }

  initLights() {
    this.frontLight.light = new THREE.PointLight( 0xFFFFFF, 1, 0, 1 );
    this.frontLight.light.color.setHSL( this.frontLight.hue, this.frontLight.saturation, 0.50 );

    if( this.client === "PHONE" ) { 
      this.topLight.light = new THREE.PointLight( 0xFFFFFF, 0, 25, 1 );
      this.topLight.light.position.set( 0, 20, -1.25 );
      this.scene.add( this.topLight.light );
    } 

    this.frontLight.light.position.set( 0, 0, 25 );
    this.scene.add( this.frontLight.light );
  }

  initRenderer() {
    this.renderer = new THREE.WebGLRenderer({ antialias : true });
    if ( this.client === "PHONE" ) {
      this.renderer.setPixelRatio( window.devicePixelRatio * 0.5 );
    }
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }


  initRaycaster() {
    this.raycaster.R = new THREE.Raycaster();
    this.raycaster.R.far = 30;
  }

  initBackground() {
    this.background.color.setHSL( 0.583, this.background.saturation, 0.19 );

    let geometry = new THREE.PlaneGeometry( 500, 500, 0);
    let material = new THREE.MeshStandardMaterial({color: this.background.color, roughness: 0.6, side: THREE.DoubleSide});

    this.background.plane = new THREE.Mesh( geometry, material );
    this.scene.add( this.background.plane );
    if ( this.client === "PHONE" ) {
      this.background.plane.position.z = -8;
    } else {
      this.background.plane.position.z = -30;
    }
    this.background.ID = this.background.plane.id;
  }

  start( id ) {
    id.appendChild( this.renderer.domElement );
  }

  animate() {
    console.log( this.exitAvail );
    this.renderer.render( this.scene, this.camera );

    window.requestAnimationFrame( this.animate );

    if ( this.client === "PHONE" ) {
      this.lightTop();

      this.socket.on('openedDoor', ( data ) => {
        this.exitAvail = data.exitAvail;
      });

      this.socket.on('closedDoor', ( data ) => {
        this.exitAvail = data.exitAvail;
      });

      this.A.pivot( this.accel.x, this.accel.y );
      //A.updateSphere(); 
      if ( this.cells.length > 0) {

        if (this.cells.length > 1) {
          this.rayCast( this );
          this.removeCells( this );
        } else {
          this.initPhysics( this );

          if ( this.exitAvail ) {

            if ( this.cells[0].position.y > 8.5 ) {
              console.log("saliendo");
              this.cells[0].transfer( this.socket );
              this.cells[0].isShowing = false;
              this.sendSound.play();
              this.removeCells( this );
              this.exitAvail = false;
            }

          }
        }

        for( let i = 0; i < this.cells.length; i++ ) {
          if(this.cells[i].mesh !== undefined) {
            this.cells[i].mesh.rotation.x += 0.02;
            this.cells[i].mesh.rotation.y += 0.015;
            this.cells[i].disappear();
          }
        }
      } else { 
        this.finish();
      }
    } else {
      // CONE 
      if (!this.receivedData) {
        this.socket.on('sent', ( data ) => {
          if (!this.receivedData) {
            console.log( data );
            this.receivedData = data;
          }
        });
      } else {
        this.getCell();
      }

      if ( this.pivot ) {
        this.pivot.rotation.x += 0.07 / 2;
        this.pivot.rotation.y += 0.017 / 2;
      } 

      if ( this.cells.length > 0) {
        for ( let i = 0; i < this.cells.length; i++ ) {
          this.cells[i].mesh.rotation.x += 0.02;
          this.cells[i].mesh.rotation.y += 0.012;
          if ( this.masterCell || this.group ) {
            this.mergeCell( i );
          }
        }
      }
      if ( this.masterCell && !this.group ) {
        this.masterCell.mesh.rotation.x += 0.02;
        this.masterCell.mesh.rotation.y += 0.012;
      }

      this.blink();
      this.doPhysics( this );
    }
  }

  water() {
    let geometry = new THREE.BoxGeometry( 100, 100, 30 );
    let material = new THREE.MeshStandardMaterial( {color: 0xffffff, transparent: true, opacity: 0.80, roughness: 1, metalness: 1} );
    let cube = new THREE.Mesh( geometry, material );
    cube.position.set( 0, 0, -14.6 );
    this.scene.add( cube );
  }

  lightTop() {
    this.topLight.light.intensity = this.topLight.intensity;

    if( this.exitAvail && this.cells.length == 1) {
      if(this.topLight.intensity < 3.5 ) {
        this.topLight.intensity += 0.03;
      } 
    } else {
      if(this.topLight.intensity > 0 ) {
        this.topLight.intensity -= 0.03;
      }
    }
  }



  mergeCell( i ) { 
    if ( this.cells[i] ) {

      const newRadius = (this.cells[i].geometry.boundingSphere.radius);

      //console.log(this.masterCell.position.distanceTo(this.cells[i].position));

      if ( !this.group ) {
        let oldRadius = (this.masterCell.geometry.boundingSphere.radius);

        if ( this.masterCell.position.distanceTo( this.cells[i].position) < ( oldRadius * 0.5 + newRadius * 0.5 )) {

          // this.masterCell.mesh.geometry.mergeMesh( this.cells[i].mesh );

          this.pivot = new THREE.Group();

          this.group = new THREE.Group();
          this.group.position.set( 0, 0, -(this.masterCell.position.z));
          this.group.add( this.masterCell.mesh );
          this.group.add( this.cells[i].mesh );

          this.scene.add ( this.group );
          this.pivot.add( this.group );
          console.log(this.pivot.position);
          console.log(this.group.position);
          console.log(this.masterCell.position);

          this.scene.add ( this.pivot );

          this.mergeSound.play();

          console.log( this.group );

          this.scene.remove(this.cells[i].mesh);
          this.cells[i].mesh.geometry.dispose();
          this.cells[i].mesh.material.dispose();
          this.cells[i].mesh = undefined;

          this.cells.splice(i, 1);  
        }
      } else { 

        this.group.children.forEach(( k ) => {
          let oldRadius = k.geometry.boundingSphere.radius;
          let center = k.geometry.boundingSphere.center;

          this.scene.updateMatrixWorld();
          let position = k.getWorldPosition().setFromMatrixPosition( k.matrixWorld );

          console.log(k);
          console.log(position.distanceTo(this.cells[i].position));

          if ( position.distanceTo(this.cells[i].position ) < ( oldRadius * 0.75 + newRadius * 0.75 )) {
            this.group.add( this.cells[i].mesh );

            this.mergeSound.play();

            this.scene.remove(this.cells[i].mesh);
            this.cells[i].mesh.geometry.dispose();
            this.cells[i].mesh.material.dispose();
            this.cells[i].mesh = undefined;

            this.cells.splice(i, 1);  
          }

        });


      }
    } else {
      return;
    }
  }

  getCell() {
    let geometry;

    switch ( this.receivedData.type ) {
      case "TetrahedronGeometry":
        geometry = new THREE.TetrahedronGeometry( 3, 0 );
        break;
      case "OctahedronGeometry":
        geometry = new THREE.OctahedronGeometry( 3, 0 );
        break;
      case "DodecahedronGeometry":
        geometry = new THREE.DodecahedronGeometry( 3, 0 );
        break;
      case "IcosahedronGeometry":
        geometry = new THREE.IcosahedronGeometry( 3, 0);
        break;
      default:
        console.log("Not any of these geometries")
        break;
    }

    geometry.vertices = this.receivedData.vertices;
    geometry.faces = this.receivedData.faces;

    geometry.computeBoundingSphere();

    let color = new THREE.Color();
    let emissive = new THREE.Color();

    color.setHSL( this.receivedData.hue, 0.9, 0.4 );
    emissive.setHSL( this.receivedData.hue, 0.5, 0.3 );

    let material = new THREE.MeshLambertMaterial({
      transparent: false,
      opacity: 1,
      color: color,
      emissive: emissive,
      flatShading: true
    });


    if( !this.masterCell && !this.group ) {
      this.masterCell = new Cell(null, null, geometry, material );
      this.masterCell.position.z = 10;
      this.masterCell.position.x = Math.random(-5, 5);
      this.masterCell.position.y = Math.random(-5, 5);
      this.masterCell.updatePosition();
      this.scene.add( this.masterCell.mesh );
    } else {
      let cell = new Cell(null, null, geometry, material );
      this.cells.push( cell );
      this.cells[this.cells.length - 1].position.z = 10;
      this.cells[this.cells.length - 1].position.x = Math.random(-5, 5);
      this.cells[this.cells.length - 1].position.y = Math.random(-5, 5);
      this.cells[this.cells.length - 1].updatePosition();
      this.scene.add( this.cells[this.cells.length - 1].mesh );
    }

    this.receiveSound.play();
    this.frontLight.blink = true;

    this.receivedData = null;
  }

  audio( scene ) {
    scene.listener = new THREE.AudioListener();
    scene.camera.add( scene.listener );

    const audioLoader = new THREE.AudioLoader();

    if ( scene.client === "PHONE" ) {
      scene.physicsSound = new THREE.Audio( scene.listener );
      audioLoader.load( 'sound/choose.wav', function( buffer ) {
        scene.physicsSound.setBuffer( buffer );
        scene.physicsSound.setLoop( false );
        scene.physicsSound.setVolume( 1 );
      });

      scene.sendSound = new THREE.Audio( scene.listener );
      audioLoader.load( 'sound/send.wav', function( buffer ) {
        scene.sendSound.setBuffer( buffer );
        scene.sendSound.setLoop( false );
        scene.sendSound.setVolume( 1 );
      });

      scene.bounceSound = new THREE.Audio( scene.listener );
      audioLoader.load( 'sound/bounce.wav', function( buffer ) {
        scene.bounceSound.setBuffer( buffer );
        scene.bounceSound.setLoop( false );
        scene.bounceSound.setVolume( 0.2 );
      });

    } else {
      scene.receiveSound = new THREE.Audio( scene.listener );
      audioLoader.load( 'sound/receive.wav', function( buffer ) {
        scene.receiveSound.setBuffer( buffer );
        scene.receiveSound.setLoop( false );
        scene.receiveSound.setVolume( 1 );
      });

      scene.mergeSound = new THREE.Audio( scene.listener );
      audioLoader.load( 'sound/merge.wav', function( buffer ) {
        scene.mergeSound.setBuffer( buffer );
        scene.mergeSound.setLoop( false );
        scene.mergeSound.setVolume( 1 );
      });

    }



  }

  rayCast( scene ) {
    this.raycaster.R.setFromCamera( this.raycaster.mouse, this.camera );

    let intersects = this.raycaster.R.intersectObjects( this.scene.children );

    if (this.raycaster.intersect == true) {
      intersects.forEach(function (i) {
        if( i.object.id == scene.background.ID ) {
          return;
        } else {
          i.object.material.transparent = true;

          scene.cells.forEach( function( cell ) { 
            cell.mesh.material.transparent = !cell.mesh.material.transparent; 
          });

          scene.raycaster.intersect = false;
        }
      });
    }
  }

  removeCells( scene ) {
    this.cells.forEach(function(cell, index, array) {
      if(cell.isShowing === false) {
        scene.scene.remove(cell.mesh);
        cell.mesh.geometry.dispose();
        cell.mesh.material.dispose();
        cell.mesh = undefined;

        array.splice(index, 1); 
      }
    });
  }

  finish () {
    this.frontLight.light.color.setHSL(this.frontLight.hue, this.frontLight.saturation, 0.50 );

    if ( this.frontLight.hue >= 0.3333333) {
      this.frontLight.hue -= 0.0025;
    }

  }


  lightBlue() {
    this.frontLight.light.color.setHSL( this.frontLight.hue, this.frontLight.saturation, 0.50 );

    if(this.frontLight.saturation < 1) {
      this.frontLight.saturation += 0.03;
    }

    if ( this.frontLight.saturation >= 1 ) {
      if ( this.frontLight.saturation > 1 ) { 
        this.frontLight.saturation = 1;
        this.physicsSound.play();
      }

    }


  }

  initPhysics( scene ) {
    this.lightBlue();
    if ( this.frontLight.saturation >= 1 ) {
      this.doPhysics( scene );
    }
  }

  doPhysics( scene ) {
    if (this.physics.frictionMag === 0) {
      this.physics.frictionMag = this.physics.frictionConstant * this.physics.frictionNormal;
    }

    if( scene.cells.length > 0 ) {
      for ( let i = 0; i < scene.cells.length; i++ ) {
        scene.cells[i].setTarget( scene.A.position);
        scene.cells[i].applyForce( scene.cells[i].target );
        scene.cells[i].applyForce( scene.A.attract( scene.cells[i] ));

        scene.physics.friction = scene.cells[i].velocity.clone();
        scene.physics.friction.negate();
        scene.physics.friction.normalize();
        scene.physics.friction.multiplyScalar( scene.physics.frictionMag );
        scene.cells[i].applyForce( scene.physics.friction );

        //console.log(scene.cells[i].position.z);
        scene.cells[i].updatePhysics( scene.client, this.bounceSound, this.exitAvail ); 
      }
    }


    // Cone
    if ( scene.masterCell ) {
      scene.masterCell.setTarget( scene.A.position);
      scene.masterCell.applyForce( scene.masterCell.target );
      scene.masterCell.applyForce( scene.A.attract( scene.masterCell ));

      scene.physics.friction = scene.masterCell.velocity.clone();
      scene.physics.friction.negate();
      scene.physics.friction.normalize();
      scene.physics.friction.multiplyScalar( scene.physics.frictionMag );
      scene.masterCell.applyForce( scene.physics.friction );

      //console.log(scene.masterCell.position.z);
      scene.masterCell.updatePhysics( scene.client, null, null); 
    }
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }

  onTouchStart( event ) {
    event.preventDefault();
    this.raycaster.mouse.x = (event.targetTouches[0].clientX / window.innerWidth) * 2 +-1;
    this.raycaster.mouse.y = -(event.targetTouches[0].clientY / window.innerHeight) * 2 + 1;
  }

  onDeviceMotion ( event ) {
    let x, y;
    x = event.accelerationIncludingGravity.x;
    y = event.accelerationIncludingGravity.y;

    this.setAccel( x, y );
  }
}

export default Scene;
