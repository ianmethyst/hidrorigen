import * as THREE from 'three';

class Attractor {
	constructor( x, y, z, mass ) {
		
		if ( x === null ) { x = 0; }
		if ( y === null ) { y = 0; }
		if ( z === null ) { z = 0; }

		this.position = new THREE.Vector3( x, y, z);

		this.G = 1;
		this.mass = mass; 

		this.theta = 0
		this.radius = 10;
	}

	showAttractor( scene ) {
		const geometry = new THREE.SphereGeometry( 0.2, 32, 32 );
		const material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
		this.sphere = new THREE.Mesh( geometry, material );

		scene.add( this.sphere );
	}

	updateSphere() {
		this.sphere.position.x = this.position.x;
		this.sphere.position.y = this.position.y;
	}

	setPosition( x, y, z ) {
		this.position = new THREE.Vector3( x, y, z );
	}

	getPosition() {
		return this.position;
	}

	attract( cell ) {
		// Direction
		let force = new THREE.Vector3().subVectors( this.position, cell.position );
		let d = force.length();
		if (d > 50) { d = 50; }
		if (d < 20) { d = 20; }
		force.normalize();

		// Magnitude
		const strength = (this.G * this.mass * cell.mass) / ( d * d);

		//console.log(strength);
		force.multiplyScalar(strength);
		return force;
	}

	pivot( x, y ) {
		this.theta = (Math.atan2( y, x ) + Math.PI);
		//console.log(this.theta);
		this.position.x = this.radius * Math.cos( this.theta );
		this.position.y = this.radius * Math.sin( this.theta );
	}
}

export default Attractor;
