import * as THREE from 'three';
class Cell {
	constructor(x, y, geometry, material ) {

		if ( !x ) { x = 0; }
		if ( !y ) { y = 0; }

		// Physics
		this.position = new THREE.Vector3( x, y, 0 );
		this.velocity = new THREE.Vector3();
		this.acceleration = new THREE.Vector3();
		this.target = new THREE.Vector3();
		this.mass = 5;

		if ( geometry !== null && material !== null) {
			this.geometry = geometry;
			this.material = material;

		} else {

			// Geometry
			this.type = getRandomInt(0, 4);
			if (this.type === 4) { this.type = 3};
			//console.log(this.type);

			switch ( this.type ) {
				case 0:
					// Tetrahedron
					this.geometry = addNoise(new THREE.TetrahedronGeometry( 1, 0 ), 0.9 ) ;
					break;
				case 1:
					// Octahedron 
					this.geometry = addNoise(new THREE.OctahedronGeometry( 1, 0 ), 0.8 );
					break;
				case 2:
					// Dodecahedron
					this.geometry = addNoise(new THREE.DodecahedronGeometry( 1, 0 ), 0.2 );
					break;
				case 3:
					// Icosahedron 
					this.geometry = addNoise(new THREE.IcosahedronGeometry( 1, 0 ), 0.5 );
					break;
				default:
					console.log("Number not in range for geometry")
					break;
			}

			// Material
			this.isDesappearing = false;
			this.isShowing = true;
			this.hue = Math.random();
			//console.log( this.hue );

			this.color = new THREE.Color();
			this.emissive = new THREE.Color();

			this.color.setHSL( this.hue, 1, 0.6 );
			this.emissive.setHSL( this.hue, 0.3, 0.2 );

			this.material = new THREE.MeshLambertMaterial( {
				transparent: false,
				opacity: 1,
				color: this.color,
				emissive: this.emissive,
				flatShading: true
			});

		}

		// Mesh
		this.mesh = new THREE.Mesh( this.geometry, this.material);

		this.updatePosition();
	}


	updatePosition() {
		this.mesh.position.x = this.position.x;
		this.mesh.position.y = this.position.y;
		this.mesh.position.z = this.position.z;
	}

	disappear() {
		if(this.material.transparent == true) {
			if( this.material.opacity > 0)
				this.material.opacity -= 0.01;
			else
				this.isShowing = false;
		}
	}

	bounce( x, y, sound, exitAvail ) { 
		if( this.position.x > x  ) {
			this.position.x = x;
			if ( sound ) {
				if (this.velocity.x > 0.01) {
					if (sound.isPlaying) { sound.stop(); }
					sound.play();
				}
			}


			this.velocity.x = this.velocity.x *= -1;
		}


		if ( this.position.x < -(x) ) { 
			this.position.x = -(x);
			this.velocity.x = this.velocity.x *= -1;

			if ( sound ) {
				if (this.velocity.x > 0.01) {
					if (sound.isPlaying) { sound.stop(); }
					sound.play();
				}
			}

		}

		if( this.position.y < -(y) ) {
			this.position.y = -(y);
			this.velocity.y = this.velocity.y *= -1;

			if ( sound ) {
				if (this.velocity.y > 0.01) {
					if (sound.isPlaying) { sound.stop(); }
					sound.play();
				}
			}
		}


		if ( exitAvail === false ) {
			if( this.position.y > y ) {
				this.position.y = (y);

				if ( sound ) {
					if (this.velocity.y > 0.01) {
						if (sound.isPlaying) { sound.stop(); }
						sound.play();
					}
				}
				this.velocity.y = this.velocity.y *= -1;
			}
		}
	}

	updatePhysics( client, sound, exitAvail ) {
		if ( client === "PHONE" ) {
			this.bounce( 5, 7.5, sound, exitAvail );
		}
    	this.velocity.add( this.acceleration );
    	this.position.add( this.velocity ); 

		this.updatePosition();

    	this.acceleration.multiplyScalar( 0 );
	}

	applyForce( vector ) {
    	const force = vector.divideScalar( this.mass );
    	this.acceleration.add( force );
	}

	setTarget( vector ) {
		this.target = new THREE.Vector3().subVectors( this.position, vector );	
		this.target.normalize();
		this.target.setLength(0.005);
	}

	transfer( socket ) {
		socket.emit('transfer', { 
			type: this.geometry.type,
			faces: this.geometry.faces,
			vertices: this.geometry.vertices,
			hue: this.hue
		});
	}

}

function getRandomInt(min, max) {
  	min = Math.ceil(min);
  	max = Math.floor(max);
  	return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function addNoise(geometry, noiseX, noiseY, noiseZ) {
    noiseX = noiseX || 2;
    noiseY = noiseY || noiseX;
    noiseZ = noiseZ || noiseY;

    // loop through each vertix in the geometry and move it randomly
    for(var i = 0; i < geometry.vertices.length; i++){
        var v = geometry.vertices[i];
        v.x += -noiseX / 2 + Math.random() * noiseX;
        v.y += -noiseY / 2 + Math.random() * noiseY;
        v.z += -noiseZ / 2 + Math.random() * noiseZ;
    }

    return geometry;
}

export default Cell;
