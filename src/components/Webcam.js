
function checkMotion( payload, boolean ) {
	if ( payload.score > 80 ) {
		boolean = true;
	} else { 
		boolean = false;
	}
}

function initSuccess() {
	DiffCamEngine.start();
}

function initError() {
	alert('Something went wrong.');
}

function capture(payload) {
	console.log(payload.score);
}

DiffCamEngine.init({
	initSuccessCallback: initSuccess,
	initErrorCallback: initError,
	captureCallback: checkMotion 
});

