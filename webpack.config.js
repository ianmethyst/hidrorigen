const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

module.exports = {
  entry: {
  	phone: ['./src/phone.js', hotMiddlewareScript],
	  cone: ['./src/cone.js', hotMiddlewareScript],
  },
  output: {
		filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
		publicPath: '/'
  },
	module: {
    rules: [
			{
      	test: /\.js$/,
      	exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        },
    	},
    	{
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
    	},
    	{
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
    	},
    	{
        test: /\.html$/,
        loader: 'html-loader'
    	}
    ]
	},
  plugins: [
    new CopyWebpackPlugin([
      { from: 'public' }
    ]),
		new CleanWebpackPlugin(['dist']),
    new webpack.HotModuleReplacementPlugin(),
  ],
	devServer: {
    contentBase: './dist'
  },
};


